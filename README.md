# Treinamento_KNN_Perceptron
Aula de KNN e Perceptron Single Layer para treinamento da Stefanini.

Aulas baseadas no curso *Deep Learning com Python de A à Z - O Curso Completo, de Jones Granatyr* (https://www.udemy.com/deep-learning-com-python-az-curso-completo/).

É recomendado utilizar os arquivos pptx para facilitar a visualização e entendimento.
